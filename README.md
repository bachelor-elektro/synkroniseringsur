# Synkroniseringsur

Synkroniseringsur for sammkøyring av to generatorar, eller av ein generator mot eit nett.

## Spesifikasjonar

Minimumskrav

* Måling av faseforskjell mellom to spenningar
* Spenningsmåling
* Frekvensmåling
* Indikasjon av faseforskjell ved hjelp av lysdiodar
* Indikasjon av spenning og frekvens ved hjelp av 7-segment display

Ekstra funksjonar

* Utlesing av data via Modbus
* Reléutgang for synkroniseringssignal

## Litteratur
